settings_table = {
-- Values for each circle
    {
        --SETTINGS FOR CPU INDICATOR BAR
        topic="${texeci 1 mosquitto_sub -C 1 -h 10.0.0.70 -t sensores/archberry/temp}",        
        bar_bottom_left_x= 10,
        bar_bottom_left_y= 150,
        bar_width= 30,
        bar_height= 100,
        --set bar background colors, 1,0,0,1 = fully opaque red
        bar_bg_red=1,
        bar_bg_green=0,
        bar_bg_blue=0,
        bar_bg_alpha=0.3,
        --set indicator colors, 1,1,1,1 = fully opaque white
        bar_in_red=1,
        bar_in_green=1,
        bar_in_blue=1,
        bar_in_alpha=1,
        max_value=100,
        unit='°C',
        nom='Temp:'
    },
    {
        --SETTINGS FOR CPU INDICATOR BAR
        topic="${texeci 1 mosquitto_sub -C 1 -h 10.0.0.70 -t sensores/archberry/ram}",        
        bar_bottom_left_x= 150,
        bar_bottom_left_y= 150,
        bar_width= 30,
        bar_height= 100,
        --set bar background colors, 1,0,0,1 = fully opaque red
        bar_bg_red=1,
        bar_bg_green=0,
        bar_bg_blue=0,
        bar_bg_alpha=0.3,
        --set indicator colors, 1,1,1,1 = fully opaque white
        bar_in_red=1,
        bar_in_green=1,
        bar_in_blue=1,
        bar_in_alpha=1,
        max_value=100,
        unit='%',
        nom='RAM: '
    },

}

-- fonts
font="Mono"
font_size=12
text="hello world"
xpos,ypos=100,100
red,green,blue,alpha=1,1,1,1
font_slant=CAIRO_FONT_SLANT_NORMAL
font_face=CAIRO_FONT_WEIGHT_NORMAL



require 'cairo'

function texto (cr,texte,pt,altura)


    cairo_select_font_face (cr, "mono", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size (cr, 12)
    cairo_set_source_rgba (cr,1,1,1,0.9)
    cairo_move_to (cr,pt['bar_bottom_left_x']+35,pt['bar_bottom_left_y']-altura)
    --cairo_show_text (cr,"hello world")
    cairo_show_text (cr,texte .. pt['unit'])
    

    cairo_select_font_face (cr, "mono", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size (cr, 12)
    cairo_set_source_rgba (cr,1,1,1,0.9)
    cairo_move_to (cr,pt['bar_bottom_left_x'],pt['bar_bottom_left_y']+20)
    --cairo_show_text (cr,"hello world")
    cairo_show_text (cr,pt['nom'])
    cairo_stroke (cr)
end

function draw_bar(cr,t,pt,val)
    local scale=pt['bar_height']/pt['max_value']    
    local indicator_height=scale*val

    --draw background
    cairo_set_source_rgba (cr,pt['bar_bg_red'],pt['bar_bg_green'],pt['bar_bg_blue'],pt['bar_bg_alpha'])
    cairo_rectangle (cr,pt['bar_bottom_left_x'],pt['bar_bottom_left_y'],pt['bar_width'],-pt['bar_height'])
    cairo_fill (cr)  

    --draw indicator
    cairo_set_source_rgba (cr,pt['bar_in_red'],pt['bar_in_green'],pt['bar_in_blue'],pt['bar_in_alpha'])--set indicator color
    --value=tonumber(conky_parse("${cpu}"))


    indicator_height=scale*val
    cairo_rectangle (cr,pt['bar_bottom_left_x'],pt['bar_bottom_left_y'],pt['bar_width'],-indicator_height)
    cairo_fill (cr)
    -- call to texto function
    texto(cr,val,pt,indicator_height)   
end


function conky_main()

    local function setup_rings(cr,pt)
        --local str=1
        --local value=1
        
        str=pt['topic']
        str=conky_parse(str)

        value=tonumber(str)

        if value == nil then
            value = 0
        end
        pct=value/pt['max_value']
        --print (value)  -- terminall test
        --print (pct)   -- terminal test
        draw_bar(cr,pct,pt,value)
    end
    




    -- Check that Conky has been running for at least 5s

    if conky_window==nil then return end

    local cs=cairo_xlib_surface_create(conky_window.display,conky_window.drawable,conky_window.visual, conky_window.width,conky_window.height)
    
    local cr=cairo_create(cs)    
    
    local updates=conky_parse('${updates}')
    update_num=tonumber(updates)
    
    if update_num>5 then
        for i in pairs(settings_table) do
           setup_rings(cr,settings_table[i])     -- USE SETTINGS_TABLE
        end
    end
    cairo_destroy(cr)
    cairo_surface_destroy(cs)
    cr=nil    
end
