settings_table = {
-- Values for each circle
    {
        dev="ArchBerry",
        nombre="Temp: ",       
        topic="${texeci 1 mosquitto_sub -C 1 -h 10.0.0.70 -t sensores/archberry/temp}",        
        max=100,
        bg_colour=0x0d4b94,
        bg_alpha=0.2,
        fg_colour=0x0d4b94,
        fg_alpha=0.5,
        x=100, y=100,
        radius=56,
        thickness=3,
        start_angle=-90,
        end_angle=180,
        unit="°C"
    },
    {
        device="ArchBerry",        
        nombre="Ram: ",
        topic="${texeci 1 mosquitto_sub -C 1 -h 10.0.0.70 -t sensores/archberry/ram}",        
        max=100,
        bg_colour=0x0d4b94,
        bg_alpha=0.2,
        fg_colour=0x0d4b94,
        fg_alpha=0.5,
        x=100, y=100,
        radius=46,
        thickness=3,
        start_angle=-90,
        end_angle=180,
        unit="%"
    },
    {
        dev="ArchBerry",
        nombre="FREQ: ",       
        topic="${texeci 1 mosquitto_sub -C 1 -h 10.0.0.70 -t sensores/archberry/freq}",        
        max=900,
        bg_colour=0x0d4b94,
        bg_alpha=0.2,
        fg_colour=0x0d4b94,
        fg_alpha=0.5,
        x=100, y=300,
        radius=56,
        thickness=3,
        start_angle=-90,
        end_angle=180,
        unit="Hz"
    },
    {
        device="ArchBerry",        
        nombre="Disk: ",
        topic="${texeci 3600 mosquitto_sub -C 1 -h 10.0.0.70 -t sensores/archberry/disco}",        
        max=100,
        bg_colour=0x0d4b94,
        bg_alpha=0.2,
        fg_colour=0x0d4b94,
        fg_alpha=0.5,
        x=100, y=300,
        radius=46,
        thickness=3,
        start_angle=-90,
        end_angle=180,
        unit="%"
    }
}

-- fonts
font="Mono"
font_size=12
text="hello world"
xpos,ypos=100,100
red,green,blue,alpha=1,1,1,1
font_slant=CAIRO_FONT_SLANT_NORMAL
font_face=CAIRO_FONT_WEIGHT_NORMAL



require 'cairo'

function texto (cr,texto,pt)
    local nom=pt['nombre']

    cairo_select_font_face (cr, "mono", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size (cr, 12)
    cairo_set_source_rgba (cr,1,1,1,0.9)
    cairo_move_to (cr,0,pt['y']+pt['radius'])
    --cairo_show_text (cr,"hello world")
    cairo_show_text (cr,nom .. texto .. pt['unit'])
    cairo_stroke (cr)

end

function draw_ring(cr,t,pt,val)

    --local w,h=conky_window.width,conky_window.height
    
    local xc,yc,ring_r,ring_w,sa,ea=pt['x'],pt['y'],pt['radius'],pt['thickness'],pt['start_angle'],pt['end_angle']
    local bgc, bga, fgc, fga=pt['bg_colour'], pt['bg_alpha'], pt['fg_colour'], pt['fg_alpha']

    local angle_0=sa*(2*math.pi/360)-math.pi/2
    local angle_f=ea*(2*math.pi/360)-math.pi/2
    local t_arc=t*(angle_f-angle_0)

    -- Draw background ring

    cairo_arc(cr,xc,yc,ring_r,angle_0,angle_f)
    cairo_set_source_rgba(cr,1,0,0,bga)
    cairo_set_line_width(cr,ring_w)
    cairo_stroke(cr)
    
    -- Draw indicator ring
    
    cairo_arc(cr,xc,yc,ring_r,angle_0,angle_0+t_arc)
    if val/pt['max']< 0.5 then 
    cairo_set_source_rgba(cr,0,0,0.8,fga)
    elseif val/pt['max'] > 0.5 and val/pt['max'] < 0.8 then
    cairo_set_source_rgba(cr,0,0,0.7,0.9)
    else
    cairo_set_source_rgba(cr,1,0,0,1)
    end
    cairo_stroke(cr)   
    -- call to texto function
    texto(cr,val,pt,nom)   
end


function conky_main()

    local function setup_rings(cr,pt)
        --local str=1
        --local value=1
        
        str=pt['topic']
        str=conky_parse(str)

        value=tonumber(str)

        if value == nil then
            value = 0
        end
        pct=value/pt['max']
        --print (value)  -- terminall test
        --print (pct)   -- terminal test
        draw_ring(cr,pct,pt,value)
    end
    




    -- Check that Conky has been running for at least 5s

    if conky_window==nil then return end

    local cs=cairo_xlib_surface_create(conky_window.display,conky_window.drawable,conky_window.visual, conky_window.width,conky_window.height)
    
    local cr=cairo_create(cs)    
    
    local updates=conky_parse('${updates}')
    update_num=tonumber(updates)
    
    if update_num>5 then
        for i in pairs(settings_table) do
           setup_rings(cr,settings_table[i])     -- USE SETTINGS_TABLE
        end
    end
    cairo_destroy(cr)
    cairo_surface_destroy(cs)
    cr=nil    
end
